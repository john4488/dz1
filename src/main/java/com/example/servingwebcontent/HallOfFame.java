package com.example.servingwebcontent;
import com.example.servingwebcontent.domain.Fame;
import org.springframework.data.repository.CrudRepository;

public interface HallOfFame  extends CrudRepository<Fame, Integer> {
}
