package com.example.servingwebcontent;

import com.example.servingwebcontent.domain.Fame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GreetingController {
    @Autowired
    private HallOfFame hallOfFame;

    @GetMapping(value="/greeting")
    public String getGreeting(Model model) {
        model.addAttribute("fame", new Fame());
        model.addAttribute("name", "Ivan?");
        return "greeting";
    }
//    @GetMapping("/greeting")
//    public String greetingDefault(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
//        model.addAttribute("name", name);
//        return "greeting";
//    }

    @PostMapping("/greeting")
    public String postGreeting(@ModelAttribute Fame fame, Model model) {
        Fame fameClone=new Fame();
        fameClone.setContent(fame.getContent());
        hallOfFame.save(fameClone);
        model.addAttribute("name", fame.getContent());
        return "greeting";
    }

    @GetMapping(value="/hall_of_fame")
    public String getHall(Model model) {
        Iterable<Fame> hall=hallOfFame.findAll();
        model.addAttribute("hall", hall);
        return "hall_of_fame";
    }

    @PostMapping("/hall_of_fame")
    public String postHall(@ModelAttribute Fame fame, Model model) {

        return "hall_of_fame";
    }

}